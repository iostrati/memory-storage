package gr.codehub.lab.memorystorage.memorystorage.repository;

import gr.codehub.lab.memorystorage.memorystorage.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    List<Car> findCarsByModelName(String modelName);

    List<Car> findCarsByReleaseDateAfter(Date releaseDate);

    List<Car> findCarsByEngineDetails(String engineDetails);
}

