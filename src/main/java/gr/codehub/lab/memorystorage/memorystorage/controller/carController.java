package gr.codehub.lab.memorystorage.memorystorage.controller;

import gr.codehub.lab.memorystorage.memorystorage.model.Car;
import gr.codehub.lab.memorystorage.memorystorage.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringReader;
import java.util.Date;
import java.util.List;

@RestController
public class carController {

    @Autowired
    private CarRepository repo;

    @GetMapping(path = "/findCarsByModel/{name}")

    public ResponseEntity<List<Car>> getName(@PathVariable String name){
        List<Car> myName =  repo.findCarsByModelName(name);
        return ResponseEntity.status(HttpStatus.OK).cacheControl(CacheControl.noCache()).body(myName);
    }

    @GetMapping(path = "/findCarsByEngineType/{type}")

    public ResponseEntity<List<Car>> getType(@PathVariable String type){
        List<Car> myType = repo.findCarsByEngineDetails(type);
        return ResponseEntity.status(HttpStatus.OK).cacheControl(CacheControl.noCache()).body(myType);
    }

    @GetMapping(path = "/findCarsByReleaseDate/{releaseDate}")

    public ResponseEntity<List<Car>> getDate(@PathVariable Date releaseDate){

        List<Car> myDate = repo.findCarsByReleaseDateAfter(releaseDate);
        return ResponseEntity.status(HttpStatus.OK).cacheControl(CacheControl.noCache()).body(myDate);
    }

    }




