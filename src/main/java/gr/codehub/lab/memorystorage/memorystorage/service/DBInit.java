package gr.codehub.lab.memorystorage.memorystorage.service;

import gr.codehub.lab.memorystorage.memorystorage.model.Car;
import gr.codehub.lab.memorystorage.memorystorage.repository.CarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private CarRepository carRepository;

    private static final Logger log = LoggerFactory.getLogger(DBInit.class);

    private static final DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        List<Car> cars = carRepository.findAll();

        if(cars.isEmpty()){
            log.info("DATABASE IS EMPTY! INITIALIZING DB...");
            Car car1 = new Car();
            Car car2 = new Car();
            Car car3 = new Car();

            car1.setModelName("Model1");
            car2.setModelName("Model2");
            car3.setModelName("Model3");

            car1.setEngineDetails("Engine1");
            car2.setEngineDetails("Engine2");
            car3.setEngineDetails("Engine3");

            try{
//                Date d1 = df.parse("10-11-2008");
//                car1.setReleaseDate(d1);
//
//                Date d2 = df.parse("28-02-2010");
//                car2.setReleaseDate(d2);
//
//                Date d3 = df.parse("06-11-2018");
//                car3.setReleaseDate(d3);

                String date1 = "10-11-2008";
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
                Date d1=formatter1.parse(date1);
                car1.setReleaseDate(d1);

                String date2 = "28-02-2010";
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
                Date d2=formatter2.parse(date2);
                car1.setReleaseDate(d2);

                String date3 = "06-11-2018";
                SimpleDateFormat formatter3 = new SimpleDateFormat("dd-MM-yyyy");
                Date d3=formatter3.parse(date3);
                car1.setReleaseDate(d3);



                carRepository.save(car1);
                carRepository.save(car2);
                carRepository.save(car3);

            }catch(ParseException e) {
                e.printStackTrace();
            }


        }
    }
}
