package gr.codehub.lab.memorystorage.memorystorage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Car {

    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "Model_Name", nullable = false)
    private String modelName;

    @Column(name = "Release_Date", nullable = false)
    private Date releaseDate;

    @Column(name = "EngineDetails", nullable = false)
    private String engineDetails;

    public Car(){
    }

    public Car(String modelName, Date releaseDate, String engineDetails){
        this.modelName = modelName;
        this.releaseDate=releaseDate;
        this.engineDetails = engineDetails;
    }

    public Long getId(){
        return id;
    }

    public  void setId(Long id){
        this.id=id;
    }

    public String getModelName(){
        return modelName;
    }

    public void setModelName( String modelName){
        this.modelName = modelName;
    }

    public Date getReleaseDate(){
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate){
        this.releaseDate = releaseDate;
    }


    public String getEngineDetails(){
        return engineDetails;
    }

    public void setEngineDetails( String engineDetails){
        this.engineDetails = engineDetails;
    }





}
