package gr.codehub.lab.memorystorage.memorystorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemorystorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemorystorageApplication.class, args);
    }
}
